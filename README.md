


睦邻通（https://moo.yodin.com）
运营实站（https://www.saoker.com）
小程序搜索：扫客

社区，电商，邻里，闲置服务，社区团购、快速开店、电商平台


# 关于


前端和后台都是一个人完成，难免有疏漏不足之处。还望大大多多指教。

后台Thinkphp6.0+VUE

如果你也喜欢并想进一步了解跟多信息，可以持续关注一下！

有任何疑问和问题欢迎留言，我会尽量抽出时间来给予解答。

# DEMO

微信小程序搜索：邻邻社

微信公众号：睦邻社

# 截图


![](https://www.saoker.com/resources/pic/L-2.jpeg)

![](https://www.saoker.com/resources/pic/L-3.jpeg)

![](https://www.saoker.com/resources/pic/L-4.jpeg)

![](https://www.saoker.com/resources/pic/L-5.jpeg)

# 在线演示及联系

![](https://www.saoker.com/resources/pic/L-11.jpeg)
